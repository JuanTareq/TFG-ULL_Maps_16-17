\beamer@endinputifotherversion {3.36pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{3}{0}{1}
\beamer@sectionintoc {2}{Herramientas y Tecnolog\IeC {\'\i }as utilizadas}{6}{0}{2}
\beamer@sectionintoc {3}{La aplicaci\IeC {\'o}n \texttt {ULL Maps{}}}{11}{0}{3}
\beamer@sectionintoc {4}{Desarrollo de \texttt {ULL Maps{}}}{20}{0}{4}
\beamer@sectionintoc {5}{Despliegue}{35}{0}{5}
\beamer@sectionintoc {6}{Presupuesto}{37}{0}{6}
\beamer@sectionintoc {7}{Summary and conclusions}{41}{0}{7}
