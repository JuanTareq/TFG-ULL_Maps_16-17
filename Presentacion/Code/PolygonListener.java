public void onPolygonClick(Polygon polygon) {
	...
	lastMarker = polyMarker.get(polygon.getId());
	lastMarker.setVisible(true);
	lastMarker.showInfoWindow();

	polCamUpd = CameraUpdateFactory
		.newLatLngZoom(lastMarker.getPosition(), gb.getBUILD_ZOOM());
	getmMap().moveCamera(polCamUpd);
	
	getMenuID().findItem(R.id.topMenu).getSubMenu().clear();
	for (String url : urls) {
	    ...
	    getMenuID().findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
	}
}

