package com.example.tareq.googlemap_tfg;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

// Background thread to save the location in remove MySQL server
public class SaveTask extends AsyncTask<LatLng, Void, Void> {
    @Override
    protected Void doInBackground(LatLng... params) {
        String lat = Double.toString(params[0].latitude);
        String lng = Double.toString(params[0].longitude);
        String strUrl = "http://85.155.38.100/tfgphp/save.php";
        URL url = null;
        try {
            url = new URL(strUrl);

            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    connection.getOutputStream());

            outputStreamWriter.write("lat=" + lat + "&lng="+lng);
            outputStreamWriter.flush();
            outputStreamWriter.close();

            InputStream iStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";

            while( (line = reader.readLine()) != null){
                sb.append(line);
            }

            reader.close();
            iStream.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
