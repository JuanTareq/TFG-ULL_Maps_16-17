package com.example.tareq.tfg_googlemaps_ull;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Tareq on 11/02/2017.
 */

public class FastAlertDialog {
    public FastAlertDialog(String msg, String pbtn, String nbtn, Context contexto) {
        AlertDialog.Builder builder = new AlertDialog.Builder(contexto);
        // Add the buttons
        if (pbtn != "") {
            builder.setPositiveButton(pbtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });
        }

        if (nbtn != "") {
            builder.setNegativeButton(nbtn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
        }

        AlertDialog dialog = builder.create();
        dialog.setMessage(msg);
        dialog.show();

    }

}
