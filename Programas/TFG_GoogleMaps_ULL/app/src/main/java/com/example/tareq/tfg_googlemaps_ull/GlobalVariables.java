package com.example.tareq.tfg_googlemaps_ull;

import android.app.Application;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tareq on 12/02/2017.
 */

public class GlobalVariables extends Application {
    //Usaremos variables para guardar las casillas en las que se encuentran los datos
    //del SpreadSheet que necesitamos.
    private static int CAMPUS = 0;
    private static int TITLE = 1;
    private static int ICON = 2;
    private static int LAT_POS = 3;
    private static int LNG_POS = 4;
    private static int DESC = 5;
    private static int URL = 6;
    private static int IMAGE = 7;
    private static int PPOINT = 8; //Aqui comienzan los puntos de los polígonos
    private static int ZOOM = 12;
    private static int BUILD_ZOOM = 17;

    public static int getLAT_POS() {
        return LAT_POS;
    }

    public static int getLNG_POS() {
        return LNG_POS;
    }

    public static int getICON() {
        return ICON;
    }

    public static int getDESC() { return DESC; }

    public static int getTITLE() { return TITLE; }

    public static int getZOOM() { return ZOOM; }

    public static int getBUILD_ZOOM() { return BUILD_ZOOM; }

    public static int getPPOINT() { return PPOINT; }

    public static int getURL() { return URL; }

    public static int getCAMPUS() { return CAMPUS; }

    public static int getIMAGE() { return IMAGE; }

}
