package com.example.tareq.tfg_googlemaps_ull;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Locale;

import static android.R.attr.description;

public class FullInfoActivity extends AppCompatActivity {
    //Creamos un HasMap para asignar a cada URL un nombre, de manera que el usuario no vea la URL
    private HashMap<String, String> option_Url = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_info);
        TextView titulo = (TextView) this.findViewById(R.id.place_title);
        TextView snippet = (TextView) this.findViewById(R.id.place_snippet);
        ImageView image = (ImageView) this.findViewById(R.id.place_icon);

        String posicion = getIntent().getExtras().get("POS").toString();
        String languageText[] = getIntent().getExtras().get("Snippet")
                .toString()
                .replace(":basico:", "")
                .split(":language:");

        titulo.setText(getIntent().getExtras().get("Titulo").toString());

        switch (Locale.getDefault().getLanguage().toString()) {
            case "es":
                snippet.setText(languageText[0] + posicion);
                break;
            case "en":
                if (languageText.length > 1)
                    snippet.setText(languageText[1] + posicion);
                else
                    snippet.setText(languageText[0] + posicion);
                break;
            case "de":
                if (languageText.length > 2)
                    snippet.setText(languageText[2] + posicion);
                else
                    snippet.setText(languageText[0] + posicion);
                break;
        }

        if (getIntent().getExtras().get("Imagen").toString().startsWith("http")) {
            Picasso.with(this)
                    .load(getIntent().getExtras().get("Imagen").toString())
                    .resize(1000, 500)
                    .into(image);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String[] urls = getIntent().getExtras().get("URLs").toString().split(";");
        String[] nameAsign;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.full_info_menu, menu);

        for (String url: urls ) {
            nameAsign = url.split("-");
            if(nameAsign.length > 1)
                option_Url.put(nameAsign[0], nameAsign[1]);
            else
                option_Url.put(nameAsign[0], nameAsign[0]);
            menu.findItem(R.id.enlaces).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String opcion = item.getTitle().toString();
        String url = option_Url.get(opcion);
        if (opcion.compareTo("back") == 0)
            finish();
        else {
            if (url != null)
                url = url.replace(" ", "");
            else
                url = "null";
            // Handle item selection
            if (url.startsWith("https:") ) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }
        return true;
    }
}
