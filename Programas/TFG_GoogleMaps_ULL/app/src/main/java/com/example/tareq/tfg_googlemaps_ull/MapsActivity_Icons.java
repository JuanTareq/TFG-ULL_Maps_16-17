package com.example.tareq.tfg_googlemaps_ull;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import static com.example.tareq.tfg_googlemaps_ull.R.layout.activity_maps__icons;

public class MapsActivity_Icons extends MapsCommon implements OnMapReadyCallback {
    private List<List<Object>> Datos;
    //Creamos un objeto de la clase GlobalVariables para acceder a variables que se comparten en toda la app
    private GlobalVariables gb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_maps__icons);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Guardamos el valor del SpreadSheet para poder utilizar la información que contiene.
        setDatos((List<List<Object>>) getIntent().getExtras().getSerializable("Datos"));
        this.Datos = getDatos();
        setTipo(1);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setmMap(googleMap);
        loadIcons();
        //Movemos la cámara para mostrar todos los campus de tenerife.
        CameraUpdate camUpd1 =
                CameraUpdateFactory
                        .newLatLngZoom(new LatLng(28.476002, -16.290341), gb.getZOOM());

        getmMap().moveCamera(camUpd1);

        //Sobreescribimos la acción al clickar sobre un marcador
        getmMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marcador) {
                String[] urls = getMarkerData().get(marcador).getUrls().split(";");
                String[] nameAsign;
                getMenuID().findItem(R.id.topMenu).getSubMenu().clear();
                for (String url: urls ) {
                    nameAsign = url.split("-");
                    if(nameAsign.length > 1)
                        getOption_Url().put(nameAsign[0], nameAsign[1]);
                    else
                        getOption_Url().put(nameAsign[0], nameAsign[0]);
                    getMenuID().findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
                }

                return false;
            }

        });

        setCustomInfoWindow(MapsActivity_Icons.this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        //Incluimos en el mapa un botón que permite mover la cámara a nuestra posición actual.
        getmMap().setMyLocationEnabled(true);
    }

    //Método encargado de inicializar la carga de los iconos
    public void loadIcons() {
        //Para cada fila del SpreadSheet
        int fila = 0;
        for (List row : getDatos()) {
            createM_Icon(row.get(gb.getLAT_POS()).toString(), row.get(gb.getLNG_POS()).toString(), fila);
            fila++;
        }
    }

    //Método encargado de crear cada icono
    public void createM_Icon(String lat, String lng, int fila) {
        //Creamos el objeto que contiene la información que necesitamos de cada marcador
        MarkersData mData = new MarkersData();
        //Convertimos las String en Double para poder crear un objeto LatLng
        LatLng posicion = new LatLng( Double.parseDouble(lat), Double.parseDouble(lng));
        String snippet = Datos.get(fila).get(gb.getDESC()).toString();
        MarkerOptions markerOptions = new MarkerOptions()
                .position(posicion)
                .title(Datos.get(fila).get(gb.getTITLE()).toString())
                .flat(true);

        if (Datos.get(fila).get(gb.getICON()) != null) {
            switch (Datos.get(fila).get(gb.getICON()).toString()) {
                case "biblio":
                    markerOptions
                            .snippet(snippet)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.biblioicon));
                    break;
                case "centro":
                    markerOptions
                            .snippet(snippet)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.centroicon));
                    break;
                case "estudio":
                    markerOptions
                            .snippet(snippet)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.estudioicon));
                    break;
                case "servicio":
                    markerOptions
                            .snippet(snippet)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.serviceicon));
                    break;
                case "instituto":
                    markerOptions
                            .snippet(snippet)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.institutoicon));
                    break;
                case "residencia":
                    markerOptions
                            .snippet(snippet)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.residenciaicon));
                    break;
                default:
                    markerOptions
                            .snippet(snippet)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.defaulticon));
                    break;
            }
        }
        Marker marcador = getmMap().addMarker(markerOptions);
        if(Datos.get(fila).size() > gb.getIMAGE() && Datos.get(fila).get(gb.getIMAGE()) != null)
            mData.setImage(Datos.get(fila).get(gb.getIMAGE()).toString());
        if(Datos.get(fila).size() > gb.getURL() && Datos.get(fila).get(gb.getURL()) != null)
            mData.setUrls(Datos.get(fila).get(gb.getURL()).toString());


        getMarkerData().put(marcador, mData);
    }
}

