package com.example.tareq.tfg_googlemaps_ull;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class MapsActivity_Poligons extends MapsCommon implements OnMapReadyCallback {
    private List<List<Object>> Datos;
    //Creamos un objeto de la clase GlobalVariables para acceder a variables que se comparten en toda la app
    private GlobalVariables gb;

    //Creamos un HashMap que contendrá el ID de cada polígono y el Marker asociado al mismo.
    private HashMap<String, Marker> polyMarker = new HashMap<String, Marker>();
    //Guardamos el valor del ultimo marcador que tendremos en visible para hacerlo invisible
    //al seleccionar otro
    private Marker lastMarker;
    private CameraUpdate polCamUpd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_poligons);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //Guardamos el valor del SpreadSheet para poder utilizar la información que contiene.
        setDatos((List<List<Object>>) getIntent().getExtras().getSerializable("Datos"));
        this.Datos = getDatos();
        setTipo(0);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setmMap(googleMap);
        loadPolygons();
        //Movemos la cámara para mostrar todos los campus de tenerife.
        final CameraUpdate camUpd1 =
                CameraUpdateFactory
                        .newLatLngZoom(new LatLng(28.476002, -16.290341), gb.getZOOM());

        getmMap().moveCamera(camUpd1);

        //Creamos un listener para los polígonos
        getmMap().setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(Polygon polygon) {
                //ocultar el ultimo marcador
                if (lastMarker != null) {
                    lastMarker.hideInfoWindow();
                    lastMarker.setVisible(false);
                }
                //Actualizamos como ultimo marcador el que vamos a mostrar
                lastMarker = polyMarker.get(polygon.getId());
                //Lo hacemos visible
                lastMarker.setVisible(true);
                //mostramos su infowindow asociada
                lastMarker.showInfoWindow();

                polCamUpd = CameraUpdateFactory
                        .newLatLngZoom(lastMarker.getPosition(), gb.getBUILD_ZOOM());
                getmMap().moveCamera(polCamUpd);

                //Actualizamos el menu
                String[] urls = getMarkerData().get(lastMarker).getUrls().split(";");
                String[] nameAsign;
                getMenuID().findItem(R.id.topMenu).getSubMenu().clear();
                for (String url : urls) {
                    nameAsign = url.split("-");
                    if (nameAsign.length > 1)
                        getOption_Url().put(nameAsign[0], nameAsign[1]);
                    else
                        getOption_Url().put(nameAsign[0], nameAsign[0]);
                    getMenuID().findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
                }

            }
        });

        setCustomInfoWindow(MapsActivity_Poligons.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Incluimos en el mapa un botón que permite mover la cámara a nuestra posición actual.
        getmMap().setMyLocationEnabled(true);
    }

    public void loadPolygons() {
        int fila = 0;
        int celda;
        LatLng centro;
        String[] punto;
        //Array que contendra todos los puntos del polígono
        ArrayList<LatLng> puntos = new ArrayList<LatLng>();

        //Para cada fila de Datos
        for (List row : Datos) {
            //Limpiamos el array list
            puntos.clear();
            //Guardamos en celda la posicion del primer punto del poligono
            celda = gb.getPPOINT();
            //Obtenemos el centro
            centro = new LatLng(Double.parseDouble(row.get(gb.getLAT_POS()).toString()),
                    Double.parseDouble(row.get(gb.getLNG_POS()).toString()));
            //Recorremos la Lista hasta el final obteniendo los puntos
            while (celda < row.size()) {
                //Comprobamos que existe una cadena para evitar errores
                if (row.get(celda).toString().compareTo("") != 0) {
                    punto = row.get(celda).toString().split(",");
                    puntos.add(new LatLng(Double.parseDouble(punto[0]),
                            Double.parseDouble(punto[1])));
                }
                celda++;
            }

            if (puntos.size() > 1) {
                createPolygon(puntos, centro, fila);
            }
            fila++;
        }
    }

    public void createPolygon(ArrayList<LatLng> puntos, LatLng centro, int fila) {
        //Creamos el objeto que contiene la información que necesitamos de cada marcador
        MarkersData mData = new MarkersData();
        //Guardamos las opciones del polígono
        PolygonOptions rectOptions = new PolygonOptions();
        for (LatLng punto : puntos) {
            rectOptions.add(punto);
        }

        rectOptions.strokeColor(Color.BLACK);
        rectOptions.fillColor(Color.argb(75, 50, 0, 255));
        rectOptions.clickable(true);
        //Creamos el polígono y añadimos
        final Polygon poligono = getmMap().addPolygon(rectOptions);

        MarkerOptions markerOptions = new MarkerOptions()
                .position(centro)
                .title(Datos.get(fila).get(gb.getTITLE()).toString())
                .flat(true)
                .snippet(Datos.get(fila).get(gb.getDESC()).toString())
                .visible(false);

        Marker marcador = getmMap().addMarker(markerOptions);
        polyMarker.put(poligono.getId(), marcador);
        if (Datos.get(fila).size() > gb.getIMAGE() && Datos.get(fila).get(gb.getIMAGE()) != null)
            mData.setImage(Datos.get(fila).get(gb.getIMAGE()).toString());
        if (Datos.get(fila).size() > gb.getURL() && Datos.get(fila).get(gb.getURL()) != null)
            mData.setUrls(Datos.get(fila).get(gb.getURL()).toString());


        getMarkerData().put(marcador, mData);
    }

}
