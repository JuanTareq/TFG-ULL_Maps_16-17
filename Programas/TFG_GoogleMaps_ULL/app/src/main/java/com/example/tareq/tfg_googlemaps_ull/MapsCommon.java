package com.example.tareq.tfg_googlemaps_ull;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Tareq on 08/03/2017.
 * Esta Clase se encarga de manejar aquello que es común a todos los mapas, como es
 * la creación del menú y sus opciones, el Callback para cargar las imágenes de las
 * infoWindow y la InfoWindow customizada que creamos para poder agregar una imagen.
 */

public class MapsCommon extends AppCompatActivity {
    private GoogleMap mMap;
    private List<List<Object>> Datos;
    private Menu menuID;
    //Creamos otro HasMap para asociar a cada edificio su localización de modo que podemos situarnos sobre ellos utilziando el menu "Campus"
    private HashMap<String, LatLng> locateBuild = new HashMap<>();
    //Creamos un HasMap para asignar a cada URL un nombre, de manera que el usuario no vea la URL
    private HashMap<String, String> option_Url = new HashMap<>();
    //HasMap que contiene la información de cada Marker externa al marcador en si
    private HashMap<Marker, MarkersData> markerData = new HashMap<>();
    //Necesitamos saber que tipo de mapa es al crear al menu para ello usaremos una variable int 0=pol, 1=icon, 2=marker
    private int tipo=0;
    private CircleOptions circleOptions = new CircleOptions()
            .strokeColor(Color.BLUE)
            .radius(30.0)
            .strokeWidth((float) 15.0);
    private Circle circle;


    public Menu getMenuID() {
        return menuID;
    }
    public GoogleMap getmMap() {
        return mMap;
    }
    public void setmMap(GoogleMap mMap) {
        this.mMap = mMap;
    }
    public HashMap<String, String> getOption_Url() {
        return option_Url;
    }
    public void setDatos(List<List<Object>> datos) {
        Datos = datos;
    }
    public HashMap<String, LatLng> getLocateBuild() {
        return locateBuild;
    }
    public List<List<Object>> getDatos() {
        return Datos;
    }
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    public HashMap<Marker, MarkersData> getMarkerData() {
        return markerData;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuID = menu;
        String campus;
        String edificio;
        LatLng posicion;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_menu, menu);
        //Primer paso
        menu.findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 0, Menu.NONE, getString(R.string.urloption));
        //Segundo paso
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 0, Menu.NONE, "Guajara");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 1, Menu.NONE, "Anchieta");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 2, Menu.NONE, "Santa Cruz");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 3, Menu.NONE, "Central");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 4, Menu.NONE, "Externos");
        //Tercer paso
        menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 5, Menu.NONE, getString(R.string.inicio_opc));
        //if (tipo != 0)
            menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 6, Menu.NONE, getString(R.string.poligonos_opc));
        //if (tipo != 1)
            menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 7, Menu.NONE, getString(R.string.iconos_opc));
        //if (tipo != 2)
            menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 8, Menu.NONE, getString(R.string.marcadores_opc));
        //Cuarto paso
        for (List row : Datos) {
            campus = row.get(GlobalVariables.getCAMPUS()).toString();
            edificio = row.get(GlobalVariables.getTITLE()).toString();
            posicion = new LatLng(Double.parseDouble(row.get(GlobalVariables.getLAT_POS()).toString()),
                    Double.parseDouble(row.get(GlobalVariables.getLNG_POS()).toString()));
            locateBuild.put(edificio, posicion);

            switch(campus) {
                case "Guajara":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(0).getSubMenu().add(edificio);
                    break;
                case "Anchieta":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(1).getSubMenu().add(edificio);
                    break;
                case "Santa Cruz":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(2).getSubMenu().add(edificio);
                    break;
                case "Central":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(3).getSubMenu().add(edificio);
                    break;
                case "Externos":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(4).getSubMenu().add(edificio);
                    break;
            }

        }

        return super.onCreateOptionsMenu(menu);
    }


    //Definimos que queremos que haga cada opcion del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int opcion = item.getItemId();
        String edificio = item.getTitle().toString();
        String url;
        LatLng posicion;
        CameraUpdate camUpd1;
        Intent intent;

        url = option_Url.get(edificio);
        posicion = getLocateBuild().get(edificio);

        switch (opcion) {
            case 6: //Poligonos
                intent = new Intent(this, MapsActivity_Poligons.class);
                intent.putExtra("Datos", (Serializable) Datos);
                startActivity(intent);
                break;
            case 7: //Iconos
                intent = new Intent(this, MapsActivity_Icons.class);
                intent.putExtra("Datos", (Serializable) Datos);
                startActivity(intent);
                break;
            case 8: //Marcadores
                intent = new Intent(this, MapsActivity.class);
                intent.putExtra("Datos", (Serializable) Datos);
                startActivity(intent);
                break;
            case 5: //Inicio
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("Datos", (Serializable) Datos);
                startActivity(intent);
                break;
            default: //Url o cambio de posicion
                if (url != null) {
                    url = url.replace(" ", "");
                    if (url.startsWith("https:")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }
                }
                else if (getLocateBuild().get(edificio) != null) {
                    camUpd1 = CameraUpdateFactory
                            .newLatLngZoom(posicion, GlobalVariables.getBUILD_ZOOM());
                    mMap.moveCamera(camUpd1);
                    if (circle != null)
                        circle.remove();
                    circleOptions.center(posicion);
                    circle = mMap.addCircle(circleOptions);
                }
        }

        return true;
    }

    //Creamos un callback para que se refresque la infoWindow al cargar la imagen
    public class InfoWindowRefresher implements Callback {
        private Marker markerToRefresh;

        InfoWindowRefresher(Marker markerToRefresh) {
            this.markerToRefresh = markerToRefresh;
        }

        @Override
        public void onSuccess() {
            markerToRefresh.showInfoWindow();
        }

        @Override
        public void onError() {}
    }

    public void setCustomInfoWindow(final Context context) {
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            //Indicamos que utilizamos el frame por defecto de la infoWindow
            @Override
            public View getInfoWindow(Marker marcador) {
                return null;
            }

            //Definimos el contenido de nuestra infoWindows personalizada
            @Override
            public View getInfoContents(Marker marcador) {

                // Obtenemos la vista del layout de nuestra infowindow personalizada
                @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.custom_infowindow, null);
                //Apuntamos a las vistas de nuestro layour para poder trabajar con ellas
                TextView title = (TextView) v.findViewById(R.id.place_title);
                TextView description = (TextView) v.findViewById(R.id.place_snippet);
                ImageView image = (ImageView) v.findViewById(R.id.place_icon);

                //Les asignamos sus valores usando el marcador
                title.setText(marcador.getTitle());

                //Seleccionamos que texto usar según el idioma
                String languageText[] = marcador.getSnippet().split(":language:");
                //Filtramos para obtener un texto básico
                String basicText[];
                switch (Locale.getDefault().getLanguage().toString()) {
                    case "es":
                        basicText = languageText[0].split(":basico:");
                        if (basicText.length > 3)
                            description.setText(basicText[1] + basicText[3] + marcador.getPosition().toString());
                        else
                            description.setText(languageText[0].replace(":basico:", "") + marcador.getPosition().toString());
                        break;
                    case "en":
                        if (languageText.length > 1) {
                            basicText = languageText[1].split(":basico:");
                            if (basicText.length > 3)
                                description.setText(basicText[1] + basicText[3] + marcador.getPosition().toString());
                            else
                                description.setText(languageText[1].replace(":basico:", "") + marcador.getPosition().toString());
                        }
                        else
                            description.setText(languageText[0].replace(":basico:", "") + marcador.getPosition().toString());
                        break;
                    case "de":
                        if (languageText.length > 2) {
                            basicText = languageText[2].split(":basico:");
                            if (basicText.length > 3)
                                description.setText(basicText[1] + basicText[3] + marcador.getPosition().toString());
                            else
                                description.setText(languageText[2].replace(":basico:", "") + marcador.getPosition().toString());
                        }
                        else
                            description.setText(languageText[0].replace(":basico:", "") + marcador.getPosition().toString() +
                                    getString(R.string.more_info));
                        break;
                }

                //Obtenemos el ancho del dispositivo para ajustar la imagen a el
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                //Manejamos el refresco de la infoWindow y cargamos las imagenes si las hay
                if (markerData.get(marcador).getImage().compareTo("") != 0) {
                    if (markerData.get(marcador).getVisited()) {
                        Picasso.with(context)
                                .load(markerData.get(marcador).getImage())
                                .resize(width, 0)
                                .into(image);
                    } else {
                        markerData.get(marcador).setVisited(true);
                        Picasso.with(context)
                                .load(markerData.get(marcador).getImage())
                                .resize(width, 0)
                                .into(image, new InfoWindowRefresher(marcador));
                    }
                }
                return v;

            }
        });

        //Abrimos una nueva actividad al clickar en una infowindow
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(context, FullInfoActivity.class);
                intent
                        .putExtra("Imagen", getMarkerData().get(marker).getImage().replace(" ", ""))
                        .putExtra("Titulo", marker.getTitle())
                        .putExtra("Snippet", marker.getSnippet())
                        .putExtra("URLs", getMarkerData().get(marker).getUrls())
                        .putExtra("POS", marker.getPosition());
                startActivity(intent);
            }
        });
    }

}
