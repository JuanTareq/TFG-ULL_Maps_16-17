public void createPolygon(ArrayList<LatLng> puntos, LatLng centro, int fila) {
    MarkersData mData = new MarkersData();
    PolygonOptions polOptions = new PolygonOptions();
    for (LatLng punto : puntos) {
        polOptions.add(punto);
    }
    polOptions.strokeColor(Color.BLACK),
              ...;
    final Polygon poligono = getmMap().addPolygon(polOptions);

    MarkerOptions markerOptions = new MarkerOptions()
            ...
            .visible(false);
    Marker marcador = getmMap().addMarker(markerOptions);
    polyMarker.put(poligono.getId(), marcador);
    ...
}
