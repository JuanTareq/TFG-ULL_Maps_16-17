public boolean onMarkerClick(Marker marcador) {
	String[] urls = getMarkerData().get(marcador).getUrls().split(";");
	String[] nameAsign;
	getMenuID().findItem(R.id.topMenu).getSubMenu().clear();
	for (String url: urls ) {
	    nameAsign = url.split("-");
	    ...
	    getMenuID().findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
	}
	return false;
}

