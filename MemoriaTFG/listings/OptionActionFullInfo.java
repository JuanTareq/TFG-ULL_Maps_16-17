public boolean onOptionsItemSelected(MenuItem item) {
    String opcion = item.getTitle().toString();
    String url = option_Url.get(opcion);
    if (opcion.compareTo("back") == 0)
        finish();
    else {
        if (url != null)
            url = url.replace(" ", "");
        else
            url = "null";
        // Handle item selection
        if (url.startsWith("https:") ) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
    }
    return true;
}
