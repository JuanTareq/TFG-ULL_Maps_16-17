    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	//Seleccionamos el layout que deseamos utilizar para esta ventana.
        setContentView(R.layout.activity_maps);
        //Obtenemos el SupportMapFragment y somos notificados cuando el mapa esta listo para ser utilizado.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //Guardamos el valor del SpreadSheet para poder utilizar la informacion que contiene.
        setDatos((List<List<Object>>) getIntent().getExtras().getSerializable("Datos"));
        this.Datos = getDatos();
	//Tipo 0 = Poligonos, tipo 1 = iconos, tipo 2 = marcadores.
        setTipo(2);
    }
