mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
{

    @Override
    public boolean onMarkerClick(Marker marcador) {
        String[] urls = getMarkerData().get(marcador).getUrls().split(";");
        String[] nameAsign;
        getMenuID().findItem(R.id.topMenu).getSubMenu().clear();
        for (String url: urls ) {
            nameAsign = url.split("-");
            if(nameAsign.length > 1)
                getOption_Url().put(nameAsign[0], nameAsign[1]);
            else
                getOption_Url().put(nameAsign[0], nameAsign[0]);
            getMenuID().findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
        }
        return false;
    }

});
