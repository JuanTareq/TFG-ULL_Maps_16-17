
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marcador) {
                return null;
            }
            //Definimos el contenido de nuestra infoWindows personalizada
            @Override
            public View getInfoContents(Marker marcador) {
                // Obtenemos la vista del layout de nuestra infowindow personalizada
                @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.custom_infowindow, null);
                //Apuntamos a las vistas de nuestro layout para poder trabajar con ellas
                TextView title = (TextView) v.findViewById(R.id.place_title);
                TextView description = (TextView) v.findViewById(R.id.place_snippet);
                ImageView image = (ImageView) v.findViewById(R.id.place_icon);
                //Les asignamos sus valores usando el marcador
                title.setText(marcador.getTitle());
                //Seleccionamos que texto usar segun el idioma
                String languageText[] = marcador.getSnippet().split(":language:");
                //Preparamos un String para quedarnos con una descripcion breve
                String basicText[];
		//Creamos un switch que depene de la lengua en la que este la app
                switch (Locale.getDefault().getLanguage().toString()) {
                    case "es":
                        basicText = languageText[0].split(":basico:");
                        if (basicText.length > 3)
                            description.setText(basicText[1] + basicText[3] + markepos +
                        getString(R.string.more_info));
                        else
                            description.setText(languageText[0].replace(":basico:", "") + markepos 				    + getString(R.string.more_info));
                        break;
                   	...
		}

                //Manejamos el refresco de la infoWindow y cargamos las imagenes si las hay
                if (markerData.get(marcador).getImage().compareTo("") != 0) {
                    if (markerData.get(marcador).getVisited()) {
                        Picasso.with(context)
                                .load(markerData.get(marcador).getImage())
                                .resize(1000,500)
                                .into(image);
                    } else {
                        markerData.get(marcador).setVisited(true);
                        Picasso.with(context)
                                .load(markerData.get(marcador).getImage())
                                .resize(1000,500)
                                .into(image, new InfoWindowRefresher(marcador));
                    }
                }
                return v;

            }
        });

        
