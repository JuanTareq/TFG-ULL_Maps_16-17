    public boolean onCreateOptionsMenu(Menu menu) {
        menuID = menu;
        String campus;
        String edificio;
        LatLng posicion;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_menu, menu);
        //Primer paso
        menu.findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 0, Menu.NONE, getString(R.string.urloption));
        //Segundo paso
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 0, Menu.NONE, "Guajara");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 1, Menu.NONE, "Anchieta");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 2, Menu.NONE, "Santa Cruz");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 3, Menu.NONE, "Central");
        menu.findItem(R.id.topMenu2).getSubMenu().addSubMenu(Menu.NONE, 4, Menu.NONE, "Externos");
        //Tercer paso
        menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 5, Menu.NONE, "Inicio");
        menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 6, Menu.NONE, "Poligonos");
        menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 7, Menu.NONE, "Iconos");
        menu.findItem(R.id.topMenu3).getSubMenu().add(Menu.NONE, 8, Menu.NONE, "Marcadores");
        //Cuarto paso
        for (List row : Datos) {
            campus = row.get(GlobalVariables.getCAMPUS()).toString();
            edificio = row.get(GlobalVariables.getTITLE()).toString();
            posicion = new LatLng(Double.parseDouble(row.get(GlobalVariables.getLAT_POS()).toString()),
                    Double.parseDouble(row.get(GlobalVariables.getLNG_POS()).toString()));
            locateBuild.put(edificio, posicion);

            switch(campus) {
                case "Guajara":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(0).getSubMenu().add(edificio);
                    break;
                case "Anchieta":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(1).getSubMenu().add(edificio);
                    break;
                case "Santa Cruz":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(2).getSubMenu().add(edificio);
                    break;
                case "Central":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(3).getSubMenu().add(edificio);
                    break;
                case "Externos":
                    menu.findItem(R.id.topMenu2).getSubMenu().getItem(4).getSubMenu().add(edificio);
                    break;
            }
        }
        return super.onCreateOptionsMenu(menu);
    }
