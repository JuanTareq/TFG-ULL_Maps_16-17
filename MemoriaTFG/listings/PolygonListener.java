mMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
    @Override
    public void onPolygonClick(Polygon polygon) {
        //ocultar el ultimo marcador
        if (lastMarker != null) {
            lastMarker.hideInfoWindow();
            lastMarker.setVisible(false);
        }
        //Actualizamos como ultimo marcador el que vamos a mostrar
        lastMarker = polyMarker.get(polygon.getId());
        //Lo hacemos visible
        lastMarker.setVisible(true);
        //mostramos su infowindow asociada
        lastMarker.showInfoWindow();

        polCamUpd = CameraUpdateFactory
                .newLatLngZoom(lastMarker.getPosition(), gb.getBUILD_ZOOM());
        getmMap().moveCamera(polCamUpd);

        //Actualizamos el menu
        String[] urls = getMarkerData().get(lastMarker).getUrls().split(";");
        String[] nameAsign;
        getMenuID().findItem(R.id.topMenu).getSubMenu().clear();
        for (String url : urls) {
            nameAsign = url.split("-");
            if (nameAsign.length > 1)
                getOption_Url().put(nameAsign[0], nameAsign[1]);
            else
                getOption_Url().put(nameAsign[0], nameAsign[0]);
            getMenuID().findItem(R.id.topMenu).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
        }

    }
});
