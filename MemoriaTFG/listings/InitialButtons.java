/* Metodos encargados de cambiar de pantalla para cada boton */

public void mapaMarkers_b(View view) {
    Intent intent = new Intent(this, MapsActivity.class);
    //Aqui es donde le anadimos a la actividad los datos del spreadsheet.
    intent.putExtra("Datos", (Serializable) Datos);
    startActivity(intent);
}

public void mapaIcons_b(View view) {
    Intent intent = new Intent(this, MapsActivity_Icons.class);
    intent.putExtra("Datos", (Serializable) Datos);
    startActivity(intent);
}

public void mapaPoligonos_b(View view) {
    Intent intent = new Intent(this, MapsActivity_Poligons.class);
    intent.putExtra("Datos", (Serializable) Datos);
    startActivity(intent);
}
  



