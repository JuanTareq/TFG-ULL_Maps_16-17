protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_full_info);
    TextView titulo = (TextView) this.findViewById(R.id.place_title);
    TextView snippet = (TextView) this.findViewById(R.id.place_snippet);
    ImageView image = (ImageView) this.findViewById(R.id.place_icon);
    String posicion = getIntent().getExtras().get("POS").toString();
    String languageText[] = getIntent().getExtras().get("Snippet")
            .toString()
            .replace(":basico:", "")
            .split(":language:");
    titulo.setText(getIntent().getExtras().get("Titulo").toString());

    switch (Locale.getDefault().getLanguage().toString()) {
        case "es":
            snippet.setText(languageText[0] + posicion);
            break;
        case "en":
            if (languageText.length > 1)
                snippet.setText(languageText[1] + posicion);
            else
                snippet.setText(languageText[0] + posicion);
            break;
      	    ... 
    }

    if (getIntent().getExtras().get("Imagen").toString().startsWith("http")) {
        Picasso.with(this)
                .load(getIntent().getExtras().get("Imagen").toString())
                .resize(1000, 500)
                .into(image);
    }

}

