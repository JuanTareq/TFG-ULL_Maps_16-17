//Metodo que actualiza las infowindow al cargarse la imagen
public class InfoWindowRefresher implements Callback {
        private Marker markerToRefresh;

        InfoWindowRefresher(Marker markerToRefresh) {
            this.markerToRefresh = markerToRefresh;
        }

        @Override
        public void onSuccess() {
            markerToRefresh.showInfoWindow();
        }

        @Override
        public void onError() {}
    }
