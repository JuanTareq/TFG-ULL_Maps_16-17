mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(context, FullInfoActivity.class);
                intent
                        .putExtra("Imagen", getMarkerData().get(marker).getImage().replace(" ", ""))
                        .putExtra("Titulo", marker.getTitle())
                        .putExtra("Snippet", marker.getSnippet())
                        .putExtra("URLs", getMarkerData().get(marker).getUrls())
                        .putExtra("POS", marker.getPosition());
                startActivity(intent);
            }
        });
