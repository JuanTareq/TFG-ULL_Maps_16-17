public void createPolygon(ArrayList<LatLng> puntos, LatLng centro, int fila) {
    //Creamos el objeto que contiene la informacion que necesitamos de cada marcador
    MarkersData mData = new MarkersData();
    //Guardamos las opciones del poligono
    PolygonOptions polOptions = new PolygonOptions();
    for (LatLng punto : puntos) {
        polOptions.add(punto);
    }

    polOptions.strokeColor(Color.BLACK),
              .fillColor(Color.argb(75, 50, 0, 255)),
              .clickable(true);
    //Creamos el poligono y se representa en el mapa
    final Polygon poligono = getmMap().addPolygon(polOptions);

    MarkerOptions markerOptions = new MarkerOptions()
            .position(centro)
            .title(Datos.get(fila).get(gb.getTITLE()).toString())
            .flat(true)
            .snippet(Datos.get(fila).get(gb.getDESC()).toString())
            .visible(false);

    Marker marcador = getmMap().addMarker(markerOptions);
    polyMarker.put(poligono.getId(), marcador);
    if (Datos.get(fila).size() > gb.getIMAGE() && Datos.get(fila).get(gb.getIMAGE()) != null)
        mData.setImage(Datos.get(fila).get(gb.getIMAGE()).toString());
    if (Datos.get(fila).size() > gb.getURL() && Datos.get(fila).get(gb.getURL()) != null)
        mData.setUrls(Datos.get(fila).get(gb.getURL()).toString());

    getMarkerData().put(marcador, mData);
}
