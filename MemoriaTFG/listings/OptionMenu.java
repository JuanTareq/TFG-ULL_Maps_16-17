public boolean onOptionsItemSelected(MenuItem item) {
    int opcion = item.getItemId();
    String edificio = item.getTitle().toString();
    String url;
    LatLng posicion;
    CameraUpdate camUpd1;
    Intent intent;

    url = option_Url.get(edificio);
    posicion = getLocateBuild().get(edificio);
    switch (opcion) {
        case 6: //Poligonos
            intent = new Intent(this, MapsActivity_Poligons.class);
            intent.putExtra("Datos", (Serializable) Datos);
            startActivity(intent);
            break;
        case 7: //Iconos
            intent = new Intent(this, MapsActivity_Icons.class);
            intent.putExtra("Datos", (Serializable) Datos);
            startActivity(intent);
            break;
        case 8: //Marcadores
            intent = new Intent(this, MapsActivity.class);
            intent.putExtra("Datos", (Serializable) Datos);
            startActivity(intent);
            break;
        case 5: //Inicio
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("Datos", (Serializable) Datos);
            startActivity(intent);
            break;
        default: //Url o cambio de posicion
            if (url != null) {
                url = url.replace(" ", "");
                if (url.startsWith("https:")) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
            }
            else if (getLocateBuild().get(edificio) != null) {
                camUpd1 = CameraUpdateFactory
                        .newLatLngZoom(posicion, GlobalVariables.getBUILD_ZOOM());
                mMap.moveCamera(camUpd1);
                if (circle != null)
                    circle.remove();
                circleOptions.center(posicion);
                circle = mMap.addCircle(circleOptions);
            }
    }

    return true;
}
