public void loadPolygons() {
    int fila = 0;
    int celda;
    LatLng centro;
    String[] punto;
    //Array que contendra todos los puntos del poligono
    ArrayList<LatLng> puntos = new ArrayList<LatLng>();

    //Para cada fila de Datos
    for (List row : Datos) {
        //Limpiamos el array list
        puntos.clear();
        //Guardamos en celda la posicion del primer punto del poligono
        celda = gb.getPPOINT();
        //Obtenemos el centro
        centro = new LatLng(Double.parseDouble(row.get(gb.getLAT_POS()).toString()),
                Double.parseDouble(row.get(gb.getLNG_POS()).toString()));
        //Recorremos la Lista hasta el final obteniendo los puntos
        while (celda < row.size()) {
            //Comprobamos que existe una cadena para evitar errores
            if (row.get(celda).toString().compareTo("") != 0) {
                //Separamos la latitud de la longitud
                punto = row.get(celda).toString().split(",");
                puntos.add(new LatLng(Double.parseDouble(punto[0]),
                        Double.parseDouble(punto[1])));
            }
            celda++;
        }
        if (puntos.size() > 1) {
            createPolygon(puntos, centro, fila);
        }
        fila++;
    }
}
