public boolean onCreateOptionsMenu(Menu menu) {
    String[] urls = getIntent().getExtras().get("URLs").toString().split(";");
    String[] nameAsign;

    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.full_info_menu, menu);

    for (String url: urls ) {
        nameAsign = url.split("-");
        if(nameAsign.length > 1)
            option_Url.put(nameAsign[0], nameAsign[1]);
        else
            option_Url.put(nameAsign[0], nameAsign[0]);
        menu.findItem(R.id.enlaces).getSubMenu().add(Menu.NONE, 1, Menu.NONE, nameAsign[0]);
    }
    return super.onCreateOptionsMenu(menu);
}
