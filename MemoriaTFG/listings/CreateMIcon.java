public void createM_Icon(String lat, String lng, int fila) {
    //Creamos el objeto que contiene la informacion que necesitamos de cada marcador
    MarkersData mData = new MarkersData();
    //Convertimos las String en Double para poder crear un objeto LatLng
    LatLng posicion = new LatLng( Double.parseDouble(lat), Double.parseDouble(lng));
    String snippet = Datos.get(fila).get(gb.getDESC()).toString();
    MarkerOptions markerOptions = new MarkerOptions()
            .position(posicion)
            .title(Datos.get(fila).get(gb.getTITLE()).toString())
            .flat(true);

    if (Datos.get(fila).get(gb.getICON()) != null) {
        switch (Datos.get(fila).get(gb.getICON()).toString()) {
            case "biblio":
                markerOptions
                        .snippet(snippet)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.biblioicon));
                break;
            case "centro":
                markerOptions
                        .snippet(snippet)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.centroicon));
                break;
            ...
            default:
                markerOptions
                        .snippet(snippet)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.defaulticon));
                break;
        }
    }
    Marker marcador = getmMap().addMarker(markerOptions);
    if(Datos.get(fila).size() > gb.getIMAGE() && Datos.get(fila).get(gb.getIMAGE()) != null)
        mData.setImage(Datos.get(fila).get(gb.getIMAGE()).toString());
    if(Datos.get(fila).size() > gb.getURL() && Datos.get(fila).get(gb.getURL()) != null)
        mData.setUrls(Datos.get(fila).get(gb.getURL()).toString());

    getMarkerData().put(marcador, mData);
}
