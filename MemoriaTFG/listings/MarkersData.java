class MarkersData {
    private String urls;
    private String image;
    private Boolean visited;

    MarkersData () {
        urls = "Sin URL";
        image = "";
        visited = false;
    }

    String getImage() { return image; }
    Boolean getVisited() { return visited; }
    String getUrls() { return urls; }
    void setImage(String image) { this.image = image; }
    void setUrls(String urls) { this.urls = urls; }
    void setVisited(Boolean visited) { this.visited = visited;}
}

