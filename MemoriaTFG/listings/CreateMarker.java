public void createMarker(String lat, String lng, int fila) {
        //Creamos el objeto que contiene la informacion que necesitamos de cada marcador.
        MarkersData mData = new MarkersData();
        //Convertimos las String en Double para poder crear un objeto LatLng.
        LatLng posicion = new LatLng( Double.parseDouble(lat), Double.parseDouble(lng));
        MarkerOptions markerOptions = new MarkerOptions()
                .position(posicion)
                .title(Datos.get(fila).get(gb.getTITLE()).toString())
                .flat(true)
                .snippet(Datos.get(fila).get(gb.getDESC()).toString());

        Marker marcador = getmMap().addMarker(markerOptions);
	//Si existe una imagen asociada al marcador la guardamos.
        if(Datos.get(fila).size() > gb.getIMAGE() && Datos.get(fila).get(gb.getIMAGE()) != null)
            mData.setImage(Datos.get(fila).get(gb.getIMAGE()).toString());
	//Si existen URL asociadas al marcador las guardamos.
        if(Datos.get(fila).size() > gb.getURL() && Datos.get(fila).get(gb.getURL()) != null)
            mData.setUrls(Datos.get(fila).get(gb.getURL()).toString());

	//Guardamos en un HashMap el objeto MarkersData asociado al marcador.
        getMarkerData().put(marcador, mData);
    }
