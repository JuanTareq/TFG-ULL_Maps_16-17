\select@language {spanish}
\contentsline {chapter}{Introducci\IeC {\'o}n}{1}{chapter*.3}
\contentsline {chapter}{\numberline {1}Objetivos}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Herramientas y Tecnolog\IeC {\'\i }as}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Herramientas de Desarrollo}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Android Studio}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}LaTeX}{4}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}GitLab}{4}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}APIs de Google}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Google Maps}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Google Sheet}{5}{subsection.2.2.2}
\contentsline {chapter}{\numberline {3}Aplicaci\IeC {\'o}n ULL-Maps}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Especificaci\IeC {\'o}n de requisitos}{8}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Explicaci\IeC {\'o}n detallada}{9}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Ventanas de la aplicaci\IeC {\'o}n}{11}{section.3.2}
\contentsline {chapter}{\numberline {4}Configuraci\IeC {\'o}n inicial y la informaci\IeC {\'o}n de la app}{14}{chapter.4}
\contentsline {section}{\numberline {4.1}Primeros pasos}{14}{section.4.1}
\contentsline {section}{\numberline {4.2}La hoja de c\IeC {\'a}lculo}{15}{section.4.2}
\contentsline {chapter}{\numberline {5}Ventana inicial y manejo de la informaci\IeC {\'o}n}{18}{chapter.5}
\contentsline {section}{\numberline {5.1}La ventana inicial}{18}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}El layout}{18}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Obtenci\IeC {\'o}n y entrega de datos}{20}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Selecci\IeC {\'o}n de idioma}{20}{subsection.5.1.3}
\contentsline {chapter}{\numberline {6}Configuraci\IeC {\'o}n com\IeC {\'u}n de los mapas}{24}{chapter.6}
\contentsline {section}{\numberline {6.1}InfoWindow personalizadas}{24}{section.6.1}
\contentsline {section}{\numberline {6.2}Dise\IeC {\~n}o del men\IeC {\'u} superior}{27}{section.6.2}
\contentsline {section}{\numberline {6.3}Control de las opciones del men\IeC {\'u} superior}{32}{section.6.3}
\contentsline {section}{\numberline {6.4}MarkersData}{33}{section.6.4}
\contentsline {chapter}{\numberline {7}Ventana \textit {FullInfo} y los mapas}{35}{chapter.7}
\contentsline {section}{\numberline {7.1}Ventana \textit {FullInfo}}{35}{section.7.1}
\contentsline {section}{\numberline {7.2}Mapas}{39}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Mapa con marcadores}{40}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Mapa con iconos}{41}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Mapa con pol\IeC {\'\i }gonos}{45}{subsection.7.2.3}
\contentsline {chapter}{\numberline {8}Conclusiones y l\IeC {\'\i }neas de trabajo futuras}{49}{chapter.8}
\contentsline {section}{\numberline {8.1}Conclusiones}{49}{section.8.1}
\contentsline {section}{\numberline {8.2}Conclusions}{50}{section.8.2}
\contentsline {chapter}{\numberline {9}Presupuesto}{51}{chapter.9}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{53}{chapter.9}
